"""
WSGI config for ask_chigan project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ask_chigan.settings")
sys.path.append('/var/www/html/ask_chigan/ask_chigan/')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
