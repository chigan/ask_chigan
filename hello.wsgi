import os
import sys
import cgi 

def application(environ, start_response):
	status = '200 OK'
	output = 'Hello World!\n' 
	
	if environ['REQUEST_METHOD'] == 'GET':
		query = cgi.parse_qs(environ['QUERY_STRING'])
		if query:
			for i, j in query.items():
				output += ''.join(i) + '    ' + ''.join(j) + '\n'
		else:
			output += 'GET = 0'
	elif environ['REQUEST_METHOD'] == 'POST':

		post_env = environ.copy()
		post_env['QUERY_STRING'] = ''
		post = cgi.FieldStorage(
			fp=environ['wsgi.input'],
			environ=post_env,
			keep_blank_values=True
		)
		for key in post.keys():
			variable = str(key)
			value = str(post.getvalue(variable))
			output += variable + '    ' +  value + "\n"
	response_headers = [('Content-type', 'text/plain'),
			('Content-Length', str(len(output)))]
	start_response(status, response_headers)
	return [output]